package it.uniba.tennisgame;

import static org.junit.Assert.*;


import org.junit.Test;

public class PlayerTest {

	
	@Test
	public void TestIncrementScore() {
		//Arrange
		Player p = new Player("Federer",0);
		
		//Act
		p.incrementScore();
		
		//Assert
		assertEquals(1,p.getScore());
	}
	
	@Test
	public void TestNotIncrementScore() {
		//Arrange
		Player p = new Player("Federer",0);
		
		//Assert
		assertEquals(0,p.getScore());
	}
	
	@Test
	public void TestScoreBeLove() {
		Player p = new Player("Federer",0);
		
		assertEquals("love",p.getScoreAsString());
	}
	
	@Test
	public void TestScoreBeFifteen() {
		Player p = new Player("Federer",1);
		
		assertEquals("fifteen",p.getScoreAsString());
	}
	
	@Test
	public void TestScoreBeThirty() {
		Player p = new Player("Federer",2);
		
		assertEquals("thirty",p.getScoreAsString());
	}
	
	@Test
	public void TestScoreBeForty() {
		Player p = new Player("Federer",3);
		
		assertEquals("forty",p.getScoreAsString());
	}
	
	@Test
	public void TestScoreBeNullButPositive() {
		Player p = new Player("Federer",5);
		
		assertEquals(null,p.getScoreAsString());
	}
	
	@Test
	public void TestScoreBeNull() {
		Player p = new Player("Federer",-1);
		
		assertEquals(null,p.getScoreAsString());
	}
	
	@Test
	public void TestDraw() {
		Player p1 = new Player("Federer",2);
		Player p2 = new Player("Nadal",2);
		
		assertEquals(true,p1.isTieWith(p2));
		
	}
	
	@Test
	public void TestHasAtLeastFourtyPoints() {
		Player p1 = new Player("Federer",3);
		
		assertEquals(true,p1.hasAtLeastFortyPoints());
	}
	
	
	@Test
	public void TestHasNotAtLeastFourtyPoints() {
		Player p1 = new Player("Federer",2);
		
		assertEquals(false,p1.hasAtLeastFortyPoints());
	}
	
	
	@Test
	public void TestHasLessThanFourtyPoints() {
		Player p1 = new Player("Federer",2);
		
		assertEquals(true,p1.hasLessThanFortyPoints());
	}
	
	@Test
	public void TestHasNotLessThanFourtyPoints() {
		Player p1 = new Player("Federer",3);
		
		assertEquals(false,p1.hasLessThanFortyPoints());
	}
	
	@Test
	public void TestHasMoreThanFortyPoints() {
		Player p1 = new Player("Federer",4);
		
		assertEquals(true,p1.hasMoreThanFourtyPoints());
	}
	
	@Test
	public void TestHasNotMoreThanFortyPoints() {
		Player p1 = new Player("Federer",3);
		
		assertEquals(false,p1.hasMoreThanFourtyPoints());
	}
	
	
	@Test
	public void TestHasOnePointAdvantage() {
		Player p1 = new Player("Federer",3);
		Player p2 = new Player("Nadal",2);
		
		assertEquals(true,p1.hasOnePointAdvantageOn(p2));
	}
	
	@Test
	public void TestHasNotOnePointAdvantage() {
		Player p1 = new Player("Federer",3);
		Player p2 = new Player("Nadal",3);
		
		assertEquals(false,p1.hasOnePointAdvantageOn(p2));
	}
	
	@Test
	public void TestHasAtLeastTwoPointsAdvantageOn() {
		Player p1 = new Player("Federer",5);
		Player p2 = new Player("Nadal",3);
		
		assertEquals(true,p1.hasAtLeastTwoPointsAdvantageOn(p2));
	}
	
	@Test
	public void TestHasNoTAtLeastTwoPointsAdvantageOn() {
		Player p1 = new Player("Federer",3);
		Player p2 = new Player("Nadal",3);
		
		assertEquals(false,p1.hasAtLeastTwoPointsAdvantageOn(p2));
	}
	
	

}
