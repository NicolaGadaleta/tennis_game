package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void TestGameNotNull() {
		Game g = null;
		
		try {
			g = new Game("Nadal","Federer");
		} catch (DuplicatedPlayerException e) {
			g = null;
		}
		
		assertNotNull(g);
	}
	
	@Test
	public void GameNull() {

		Game g = null;
		
		try {
			g = new Game("Nadal","Nadal");
		} catch (DuplicatedPlayerException e) {
			g = null;
		}
		
		assertNull(g);
	}
	
	@Test
	public void TestStatusFifteenThirty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		String status = g.getGameStatus();
		
		assertEquals("Federer fifteen - Nadal thirty",status);
	}
	
	@Test
	public void TestStatusFortyThirty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		String status = g.getGameStatus();
		
		assertEquals("Federer forty - Nadal thirty",status);
	}
	
	@Test
	public void TestStatusFifteenForty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");

		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		String status = g.getGameStatus();
		
		assertEquals("Federer fifteen - Nadal forty",status);
	}
	
	@Test
	public void TestDeuce() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");

		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		String status = g.getGameStatus();
		
		assertEquals("Deuce",status);
	}
	
	@Test
	public void TestAdvantagePlayer1() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");

		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		
		g.incrementPlayerScore("Federer");
		
		String status = g.getGameStatus();
		
		assertEquals("Advantage Federer",status);
	}

	
	@Test
	public void TestAdvantagePlayer2() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");

		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		
		g.incrementPlayerScore("Federer");
		
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		String status = g.getGameStatus();
		
		assertEquals("Advantage Nadal",status);
	}
	
	
	@Test
	public void TestPlayer1Wins() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
	
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
		String status = g.getGameStatus();
		
		assertEquals("Federer wins",status);
	}
	
	@Test
	public void TestDuceAfterAdvantage() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
	
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Federer");
		String status = g.getGameStatus();
		
		assertEquals("Deuce",status);
	}

	@Test
	public void TestPlayer2Wins() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		Game g = new Game("Federer","Nadal");
		
		g.incrementPlayerScore("Federer");
		g.incrementPlayerScore("Federer");
	
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		
		g.incrementPlayerScore("Nadal");
		g.incrementPlayerScore("Nadal");
		String status = g.getGameStatus();
		
		assertEquals("Nadal wins",status);
	}
}
